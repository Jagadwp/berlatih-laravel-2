<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Question;

class PertanyaanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->only(['destroy']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        // $question = DB::table('question')->get();
        $question = Question::all();

        return view('pertanyaan.index', compact('question'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pertanyaan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'isi' => 'required'
        ]);
            
        // $query = DB::table("question")->insert([
        //     "judul" => $request["judul"], 
        //     "isi" => $request["isi"]
        // ]);

        Question::create($request->all());
        
        return redirect('/pertanyaan')->with('success', 'New question created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $question = DB::table('question')->where('id', $id)->first();
        $question = Question::find($id);

        return view('pertanyaan.show', compact('question'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $question = DB::table('question')->where('id', $id)->first();
        $question = Question::find($id);

        return view('pertanyaan.edit', compact('question'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $request->validate([
            'judul' => 'required',
            'isi' => 'required'
        ]);

        $question = Question::where('id', $id)
        ->update([
            'judul' => $request->judul,
            'isi' => $request->isi
        ]);
        
        return redirect('/pertanyaan')->with('success', 'Question successfully edited');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $query = DB::table('question')->where('id', $id)->delete();
        Question::destroy($id);
        
        return redirect('/pertanyaan')->with('success', 'Question successfully deleted');
    }
}