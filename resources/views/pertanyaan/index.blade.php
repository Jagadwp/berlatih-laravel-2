@extends('adminlte.master')

@section('content')
    <div class="">
        <div class="card">
            <div class="card-header">
              <h3 class="card-title">Questions Table</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                <a href="/pertanyaan/create" class="btn btn-primary mb-3">Create New Question</a>
                <table class="table table-bordered">
                    <thead>                  
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Title</th>
                            <th>Content</th>
                            <th style="width: 40px">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($question as $key => $value)
                            <tr>	
                                <td>{{ $loop->iteration }}</th>
                                <td>{{ $value->judul }}</td>
                                <td>{{ $value->isi }}</td>
                                <td style="display: flex">
                                    <a href="/pertanyaan/{{ $value->id }}" class="btn btn-info mx-1">Show</a>
                                    <a href="/pertanyaan/{{ $value->id }}/edit" class="btn btn-default mx-1">Edit</a>
                                    <form action="/pertanyaan/{{ $value->id }}" method="POST" class="mx-1">
                                        @csrf
                                        @method('DELETE')
                                        <input type="submit" class="btn btn-danger" value="Delete">
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr colspan="3">
                                <td>No data</td>
                            </tr>  
                        @endforelse  
                    </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
    </div>
@endsection