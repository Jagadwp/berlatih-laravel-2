@extends('adminlte.master')

@section('content')
    <div class="ml-3">
        <h2>Detail Question {{ $question->id }}</h2>
        <hr>
        <h4>{{ $question->judul }}</h4>
        <p>{{ $question->isi }}</p>
    </div>
@endsection